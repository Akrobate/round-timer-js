'use strict'

export default {
    START_DELAY: 5,
    END_ROUND_DURATION: 30,

    REST_SHORT_DURATION: 30,
    REST_LONG_DURATION: 60,

    ROUND_SHORT_DURATION: 2 * 60,
    ROUND_LONG_DURATION: 3 * 60,

    RED_LAMP_COLOR: '#FF0000',
    ORANGE_LAMP_COLOR: '#FFA500',
    GREEN_LAMP_COLOR: '#00FF00',
}
